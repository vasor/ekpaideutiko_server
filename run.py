#! /usr/bin/env python3
# -*- coding: utf-8 -*-
import os
import sys
import config
import jwt
import requests
import re
import time
import schedule
import random
from bson.objectid import ObjectId
from functools import wraps
from flask import Flask, jsonify, request, g
from werkzeug.security import generate_password_hash, check_password_hash
from datetime import datetime
from datetime import timedelta
from flask_cors import cross_origin
from jwt import DecodeError, ExpiredSignature
from decorators import login_required
from threading import Timer
from datetime import timedelta
from flask_cors import cross_origin
from werkzeug.security import generate_password_hash, check_password_hash
from pymongo import MongoClient


app = Flask(__name__)

BASE_DIR = os.path.abspath(os.path.dirname(__file__))
sys.path.append(BASE_DIR)

activate_this = os.path.join(BASE_DIR, 'venv/bin/activate_this.py')
exec(open(activate_this).read())

print("ready")

# Connection with localhost and mongo


def get_mongo_client():
    return MongoClient('mongodb://127.0.0.1:27017/elearningapp')
client = get_mongo_client()
userscol = client.elearningapp.users
theorycol = client.elearningapp.theory
wallcol = client.elearningapp.wall
catcol = client.elearningapp.categories
exescol = client.elearningapp.exercises
statscol = client.elearningapp.statistics


def create_token(user_id):
    payload = {
        'sub': user_id,
        'iat': datetime.utcnow(),
        'exp': datetime.utcnow() + timedelta(days=14)
    }
    token = jwt.encode(payload, config.JWT_SECRET_KEY)
    return token.decode('unicode_escape')


# ========= ROUTES ==========


@app.route('/login', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def login():
    username = request.json.get('username')
    password = request.json.get('password')

    found = userscol.find_one({'username': username})

    if not found:
        response = jsonify(message='Not existent username')
        response.status_code = 404
        return response

    if check_password_hash(found['password'], password):
        token = create_token(str(found['_id']))
        return jsonify(token=token)
    else:
        response = jsonify(message='Wrong username or Password')
        response.status_code = 401
        return response


@app.route('/signup', methods=['POST', 'GET'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def signup():
    data = request.get_json()
    name = data['firstname']
    surname = data['lastname']
    username = data['username']
    email = data['email']
    password = data['password']
    gender = data['gender']

    if not name:
        response = jsonify(message='No name inserted')
        response.status_code = 401
        return response

    if not surname:
        response = jsonify(message='No surname inserted')
        response.status_code = 401
        return response

    if not username:
        response = jsonify(message='No username inserted')
        response.status_code = 401
        return response

    if not email:
        response = jsonify(message='No email inserted')
        response.status_code = 401
        return response

    if not password:
        response = jsonify(message='No password inserted')
        response.status_code = 401
        return response

    if userscol.find_one({'email': email}):
        response = jsonify(message='Email already in use')
        response.status_code = 401
        return response

    if userscol.find_one({'username': username}):
        response = jsonify(message='username already in use')
        response.status_code = 401
        return response

    password = generate_password_hash(password)
    user_id = userscol.insert({'name': name, 'surname': surname, 'username': username,
                               'email': email, 'password': password, 'registered_at': datetime.now(), 'gender': gender})
    token = create_token(str(user_id))
    print(token)
    return jsonify(token=token)


@app.route('/getTheory', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def get_theory():
    category = request.json.get('category')

    found = [theory for theory in theorycol.find(
        {'category': category}, {'_id': 1, 'title': 1, 'description': 1, 'category': 1})]
    for i in range(0, len(found)):
        found[i]['_id'] = str(found[i]['_id'])
    ratings = catcol.find_one(
        {'category_name': category}, {'_id': 0, 'cat_id': 0, 'category_name': 0})
    if not found:
        response = jsonify(message='Not existent category')
        response.status_code = 404
        return response
    else:
        return jsonify(data=found, ratings=ratings)


@app.route('/rateUp', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def rate_up():
    category = request.json.get('category')
    catcol.update_one({'category_name': category}, {'$inc': {'up': 1}})
    return jsonify(message="updated")


@app.route('/rateDown', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def rate_down():
    category = request.json.get('category')
    catcol.update_one({'category_name': category}, {'$inc': {'down': 1}})
    return jsonify(message="updated")


@app.route('/deleteContent', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def delete_content():
    data = request.get_json()

    iid = data['id']
    theorycol.remove({'_id': ObjectId(iid)})
    return jsonify(message='theory item has been deleted')


@app.route('/addContent', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def add_content():
    data = request.get_json()
    title = data['title']
    description = data['description']
    category = data['category']
    theorycol.insert(
        {'title': title, 'description': description, 'category': category})
    return jsonify(message='theory item has been added')


@app.route('/editContent', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def update_content():
    data = request.get_json()
    theorycol.update_one({'_id': ObjectId(data['id'])}, {'$set': data})
    return jsonify(message='theory item has been updated')


@app.route('/info', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def getinfo():
    user_id = request.get_json()['user_id']
    if (user_id == 'admin'):
        found = userscol.find_one({'_id': 'admin'}, {
            '_id': 0, 'password': 0})
    else:
        found = userscol.find_one({'_id': ObjectId(user_id)}, {
            '_id': 0, 'password': 0})

    response = jsonify(username=found['username'], name=found['name'], surname=found[
                       'surname'], email=found['email'], registered_at=found['registered_at'])
    return response


@app.route('/loadWallData', methods=['GET'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def load_wall_data():
    return jsonify([item for item in wallcol.find(
        {}, {'_id': 0})])


@app.route('/addPost', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def add_post():
    data = request.get_json()
    username = data['username']
    post = data['post']
    wallcol.insert(
        {'username': username, 'post': post, 'added': datetime.now()})
    return jsonify(message='post added')


@app.route('/search', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def search():
    data = request.get_json()
    search = data['search']
    filtr = data['filter']
    print(data)
    if filtr == "0":
        print("case0")
        results = [user for user in userscol.find({"$or": [{"username": {'$regex': re.compile(".*" + search + ".*")}}, {
                                                  "name": {'$regex': re.compile(".*" + search + ".*")}}]}, {'_id': 0, 'password': 0})]

    elif filtr == "1":
        print("case1")
        results = [user for user in userscol.find({"$or": [{"username": {'$regex': re.compile(".*" + search + ".*")}}, {
                                                  "name": {'$regex': re.compile(".*" + search + ".*")}}], "gender": 'κορίτσι'}, {'_id': 0, 'password': 0})]
    else:
        print("case2")
        results = [user for user in userscol.find({"$or": [{"username": {'$regex': re.compile(".*" + search + ".*")}}, {
            "name": {'$regex': re.compile(".*" + search + ".*")}}], "gender": 'αγόρι'}, {'_id': 0, 'password': 0})]

    print(results, "length: ", len(results))
    if len(results) == 1:
        if results[0]['username'] == 'admin':
            results.pop(0)
    elif len(results) > 0:
        for i in range(0, len(results) - 1):
            print("i: ", i)
            if results[i]['username'] == 'admin':
                results.pop(i)
    return jsonify(results)


@app.route('/getProfileInfo', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def get_profile_info():
    username = request.get_json()['username']
    if (username == 'admin'):
        response = jsonify(message='Not valid username')
        response.status_code = 404
    else:
        found = userscol.find_one({'username': username}, {'password': 0})

    found['_id'] = str(found['_id'])
    # print(found)
    response = jsonify(id=found['_id'], username=found['username'], name=found['name'], surname=found[
                       'surname'], email=found['email'], registered_at=found['registered_at'])
    return response


@app.route('/loadTests', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def load_tests():
    category = request.get_json()['category']
    user_id = request.get_json()['user_id']
    found = [item for item in exescol.find(
        {'category': category}, {'_id': 1, 'op1': 1, 'op2': 1, 'op3': 1, 'operator': 1, 'category': 1, 'result': 1})]
    for i in range(0, len(found)):
        found[i]['_id'] = str(found[i]['_id'])
    rnd = []
    if user_id == 'admin':
        return jsonify(found)
    else:
        if len(found) > 10:
            rnd = random.sample(range(0, len(found)), 10)
            results = []
            for item in rnd:
                results.append(found[item])
            print(len(results), " APOTELESMATA")
            return jsonify(results)
        else:
            print(len(found), " APOTELESMATA")
            return jsonify(found)


@app.route('/generalTests', methods=['GET'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def load_general_tests():
    found = [item for item in exescol.find(
        {}, {'_id': 1, 'op1': 1, 'op2': 1, 'op3': 1, 'operator': 1, 'category': 1, 'result': 1})]
    for i in range(0, len(found)):
        found[i]['_id'] = str(found[i]['_id'])

    r = {}
    for result in found:
        # print(result)
        key = result['category']
        if key not in r:
            r[key] = []
        r[key].append(result)

    todelete = []
    results = []
    remaining = 20
    for row in r:
        if len(r[row]) <= 5:
            for i in r[row]:
                # print("i: ", i)
                results.append(i)

        else:
            rnd = random.sample(range(0, len(r[row])), 5)
            # print("rnd: ", rnd)
            for rd in rnd:
                # print("rd: ", rd)
                # print("r[row]: ", r[row])
                # print("r[row][rd]: ", r[row][rd])
                results.append(r[row][rd])
    print("RESULTS LENGTH BEFORE:", len(results))
    print("FOUND LENGTH BEFORE: ", len(found))

    # Αυτη τη στιγμη εχουμε παρει 5 απο καθε κατηγορια ασκησεων.
    # Αν καποια κατηγορια έχει λιγότερες από 5, τότε συμπληρώνονται
    # οι 20 που θέλουμε από όποια έχει περίσσευμα. Αν δεν υπάρχει περίσσευμα,
    # παίρνουμε όσες ερωτήσεις έχουμε

    if (len(results) < 20):
        print("LIGOTERA APO 20")
        todelete = results
        # print("TODELETE: ", todelete)
        for td in todelete:
            for f in found:
                if td == f:
                    found.remove(f)
        print("FOUND LENGTH AFTER: ", len(found))
        diafora = 20 - len(results)
        print("THELOYME AKOMA: ", diafora)
        # Αν οι ερωτήσεις που περισσεύουν είναι μικρότερες ή ίσες από τη διαφορά, βάλτες όλες
        # Αλλιώς, βάλε τυχαία τόσες όσες και η διαφορά
        if len(found) <= diafora:
            print("FOUND<=DIAFORA")
            for f in found:
                results.append(f)
        else:
            print("FOUND>DIAFORA")
            rnd = random.sample(range(0, len(found)), diafora)
            print("RANDOM: ", rnd)
            for rd in rnd:
                results.append(found[rd])

        # print(found)
    print("RESULTS LENGTH AFTER: ", len(results))

    return jsonify(results)


@app.route('/saveAnswer', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def save_answer():
    exercise_id = request.get_json()['exercise_id']
    user_id = request.get_json()['user_id']
    answer = request.get_json()['success']
    category = request.get_json()['category']
    diagnosis = request.get_json()['diagnosis']
    statscol.insert({'exercise_id': ObjectId(exercise_id),
                     'user_id': ObjectId(user_id), 'answer': answer, 'category': category, 'diagnosis': diagnosis})

    if answer == 'ΣΩΣΤΑ':
        userscol.update_one({'_id': ObjectId(user_id)},
                            {'$inc': {'count': -1}})
        res = userscol.find_one({'_id': ObjectId(user_id)},
                                {'count': 1})
        # print(count, type(count))
        if res['count'] == 0:
            userscol.update_one({'_id': ObjectId(user_id)}, {
                                '$set': {'diagnosis': 'ΚΑΜΙΑ'}})

    else:
        userscol.update_one({'_id': ObjectId(user_id)}, {
                            '$set': {'count': 3, 'diagnosis': diagnosis}})

    # print(exercise_id, user_id, answer)
    return jsonify(message="saved")


@app.route('/deleteExercise', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def delete_exercise():
    data = request.get_json()

    iid = data['id']
    exescol.remove({'_id': ObjectId(iid)})
    return jsonify(message='exercise has been deleted')


@app.route('/addExercise', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def add_exercise():
    data = request.get_json()

    op1 = data['op1']
    op2 = data['op2']
    op3 = data['op3']
    category = data['category']
    operator = data['operator']
    result = data['result']
    exescol.insert({'op1': op1, 'op2': op2, 'op3': op3,
                    'operator': operator, 'result': result, 'category': category})
    return jsonify(message='exercise has been added')


@app.route('/statistics', methods=['POST'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def statistics():
    user_id = request.get_json()['user_id']
    results = [result for result in statscol.find(
        {'user_id': ObjectId(user_id)}, {'_id': 0})]
    d = userscol.find_one({'_id': ObjectId(user_id)}, {'_id': 0})
    print(d)
    statistics = return_stats(results)
    if 'diagnosis' in d:
        statistics['count'] = d['count']
        statistics['diagnosis'] = d['diagnosis']
    return jsonify(statistics)
    # , jsonify(diagnosis=diagnosis, count=count)


@app.route('/all_statistics', methods=['GET'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def all_statistics():

    results = [result for result in statscol.find(
        {}, {'_id': 0})]

    statistics = return_stats(results)
    return jsonify(statistics)


@app.route('/girls_statistics', methods=['GET'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def girls_statistics():

    results = [result for result in statscol.find(
        {}, {'_id': 0})]

    r = {}
    for result in results:

        key = result['user_id']
        if key not in r:
            r[key] = []
        r[key].append(result)

    todelete = []
    for row in r:
        found = userscol.find_one({"_id": ObjectId(row)}, {'_id': 0})
        if found['gender'] != u'κορίτσι':
            todelete.append(row)
    for item in todelete:
        del r[item]

    # καθαρισμα της λιστας
    results = []

    for item in r:
        for i in r[item]:
            results.append(i)

    statistics = return_stats(results)
    return jsonify(statistics)


@app.route('/boys_statistics', methods=['GET'])
@cross_origin(headers=['Content-Type', 'Authorization'])
def boys_statistics():

    results = [result for result in statscol.find(
        {}, {'_id': 0})]

    r = {}
    for result in results:

        key = result['user_id']
        if key not in r:
            r[key] = []
        r[key].append(result)

    todelete = []
    for row in r:
        found = userscol.find_one({"_id": ObjectId(row)}, {'_id': 0})
        if found['gender'] != u'αγόρι':
            todelete.append(row)

    for item in todelete:
        del r[item]

    # καθαρισμα της λιστας
    results = []

    for item in r:
        for i in r[item]:
            results.append(i)

    statistics = return_stats(results)
    return jsonify(statistics)


def return_stats(results):
    addition = []
    substraction = []
    division = []
    multiplication = []

    general_answered_right = 0
    multiplication_answered_right = 0
    addition_answered_right = 0
    substraction_answered_right = 0
    division_answered_right = 0

    for i in range(0, len(results)):
        results[i]['exercise_id'] = str(results[i]['exercise_id'])
        results[i]['user_id'] = str(results[i]['user_id'])

        if results[i]['answer'] == "ΣΩΣΤΑ":
            general_answered_right = general_answered_right + 1

        if results[i]['category'] == 'πρόσθεση':
            addition.append(results[i])
            if results[i]['answer'] == "ΣΩΣΤΑ":
                addition_answered_right = addition_answered_right + 1
        elif results[i]['category'] == 'αφαίρεση':
            substraction.append(results[i])
            if results[i]['answer'] == "ΣΩΣΤΑ":
                substraction_answered_right = substraction_answered_right + 1
        elif results[i]['category'] == 'διαίρεση':
            division.append(results[i])
            if results[i]['answer'] == "ΣΩΣΤΑ":
                division_answered_right = division_answered_right + 1
        elif results[i]['category'] == 'πολλαπλασιασμός':
            multiplication.append(results[i])
            if results[i]['answer'] == "ΣΩΣΤΑ":
                multiplication_answered_right = multiplication_answered_right + 1

    # print(addition, substraction, division, multiplication)

    statistics = {'general_questions_answered': len(results), 'general_answered_right': general_answered_right,
                  'addition_questions_answered': len(addition), 'substraction_questions_answered': len(substraction), 'division_questions_answered': len(division),
                  'multiplication_questions_answered': len(multiplication), 'addition_answered_right': addition_answered_right, 'substraction_answered_right': substraction_answered_right,
                  'division_answered_right': division_answered_right, 'multiplication_answered_right': multiplication_answered_right}
    return statistics


# STARTS APPLICATION

if __name__ == '__main__':
    app.run(
        host=config.HOST,
        port=config.PORT,
        debug=config.DEBUG,
        threaded=config.THREADED
    )
